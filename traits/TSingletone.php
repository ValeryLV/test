<?php

namespace app\traits;


use app\engine\Db;

trait TSingletone
{
    //изначльно был в Dv.php (перенес т.к везде используется
    //будем хранить класс  Db внутри себя
    private  static $instance = null;

    //чтобы подключаться в единсвенном экземпляре а не каждый раз

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new Db();
        }
        return static::$instance;
    }

    //закрываем конструктор чтобы кто-нибудь не сделал вью
    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}