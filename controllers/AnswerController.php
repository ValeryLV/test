<?php


namespace app\controllers;

use app\engine\Auth;
use app\model\Answers;
use app\model\Questions;
use app\model\Quiz;

class AnswerController extends RenderController
{
    //CRUD

    //добавление ответа
    public function actionSave()
    {
        $questions_id = (int)$_POST['questions_id'];
        $question = Questions::getOne($questions_id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $title = (string)$_POST['title'];
        $is_true = (int)$_POST['is_true'];
        $answers = new Answers($title, $questions_id, $is_true);
        $answers->save();

        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

    //изменение ответа
    public function actionStore()
    {
        $answers = Answers::getOne($_POST['id']);
        $question = Questions::getOne($answers->question_id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $answers->text = $_POST['text'];
        $answers->is_true = $_POST['is_true'];
        $answers->save();

        header("Location: /questions/update/?id=" . $_POST['questions_id']);
    }

    //обновление ответа рендер странцы для редактирвоания
    public function actionUpdate()
    {
        $answers = Answers::getOne($_GET['id']);
        $question = Questions::getOne($answers->question_id);
        $quiz = Quiz::getOne($question->quiz_id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/answers/edit', [
            'id' => $answers->id,
            'text' => $answers->text,
            'question_id' => $answers->question_id,
            'is_true' => $answers->is_true,
            'slug' => $quiz->slug,
        ]);
    }

    public function actionDelete()
    {
        $answers = Answers::getOne($_GET['id']);
        $question = Questions::getOne($answers->question_id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $status = $answers->delete();

        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
}