<?php


namespace app\controllers;

use app\engine\Auth;
use app\model\Answers;
use app\model\Questions;
use app\model\Quiz;
use const http\Client\Curl\AUTH_GSSNEG;

class QuestionsController extends RenderController
{
    public function actionOne()
    {
        if (!Auth::isAdmin() && !Auth::isModerator() && !Auth::isUser()) {
            echo $this->render('errors/login'); die();
        }

        $page = $_GET['page'];

        //получение id викторины
        $id = (int)$_GET['id'];

        //получение вопроса
        $questions = Questions::getLimitWhere('quiz_id', $id, $page ?? 0);

        if ($page === '0') {
            //Старт викторины
            $_SESSION['answers'] = [];
            $_SESSION['currentQuizId'] = (int)$_GET['id'];
            $_SESSION['currentQuestionsId'] = $questions->id;
            $_SESSION['quizHash'] = uniqid(rand(), true);
            $_SESSION['date'] = date('y-m-d h:i:s');
        }

        //защита от дурака
        if ($id != $_SESSION['currentQuizId'] || $questions->id != $_SESSION['currentQuestionsId'])
        {
            echo $this->render('errors/error', [
                'id_questions' => $_SESSION['currentQuizId'],
                'page' => $_GET['page']
                ]); die();
        }

        if (isset($_POST['ok'])) {
            //принять ответы

            if (count($_POST) == 1) {
                header("Location: " . $_SERVER['HTTP_REFERER']);
                die();
            }
            foreach ($_POST as $key => $answer) {
                if ($key == 'ok') continue;
                $_SESSION['answers'][] = $key;
            }
            //обновление id вопроса для защиты (смотрим будущее)
            $questions = Questions::getLimitWhere('quiz_id', $id, $page + 1);
            $_SESSION['currentQuestionsId'] = $questions->id;

            header("Location: /questions/one/?id={$id}&page=" . ++$page);

            die();
        }
        if (!$questions) {
            header("Location: /result");
            die();
        ////завершение опроса header('результаты');
        }
        //получение вариантов ответа
        $answers = Answers::getAllWhere('question_id', $questions->id, 'rand');

        echo $this->render('questions', [
            'questions' => $questions,
            'answers' => $answers,
            'id' => $id,
            'count' => $page + 1,
            'countAll' => Questions::getCountAll($id)
        ]);
    }

    //CRUD
    //рендер странцы для добавления вопроса с получением id викторины
    public function actionAdd()
    {
        $id = (int)$_GET['id'];

        if (!Auth::isAdmin() && !(Auth::isModerator() && $id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/questions/add', [
            'quiz_id' => $id,
        ]);
    }

    //обновление вопроса рендер странцы для редактирвоания
    public function actionUpdate()
    {
        $questions = Questions::getOneQuestion($_GET['id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $questions->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $quiz = Quiz::getOne($questions->quiz_id);

        $answers = Answers::getAllWhere('question_id', $questions->id);

        echo $this->render('admin/questions/edit', [
            'id' => $questions->id,
            'title' => $questions->question,
            'quiz_id' => $questions->quiz_id,
            'slug' => $quiz->slug,
            'answers' => $answers
        ]);
    }

    //создание нового вопроса по id викторины
    public function actionSave()
    {
        $id = $_POST['quiz_id'];
        $question = new Questions($_POST['question'], $_POST['quiz_id']);
        $quiz = Quiz::getOne($id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $question->save();

        //(new Answers($_POST['answers-1'], $question_id, $_POST['is_true']))->save();
        header("Location: /quiz/update/" . $quiz->slug);
    }

    //измение вопроса
    public function actionStore()
    {
        $id = (int)$_POST['quiz_id'];
        $question = Questions::getOne($_POST['id']);
        $quiz = Quiz::getOne($id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $question->question = $_POST['title'];

        $question->save();

        header("Location: /quiz/update/" . $quiz->slug);
    }

    //удаление вопроса по его id
    public function actionDelete()
    {

        $questions = Questions::getOne($_GET['id']);
        $quiz = Quiz::getOne($questions->quiz_id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $questions->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/questions/confirmDelete', [
            'title' => $questions->question,
            'quiz_id' => $questions->quiz_id,
            'slug' => $quiz->slug,
            'id' => $questions->id,
        ]);
    }

    //подтверждение удаления вопроса
    public function actionDeleteConfirmed()
    {
        $id = $_GET['quiz_id'];
        $question = Questions::getOne($_GET['id']);
        $quiz = Quiz::getOne($id);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $question->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $status = $question->delete();

        header("Location: /quiz/update/" . $quiz->slug);
    }
} 