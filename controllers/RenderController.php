<?php


namespace app\controllers;


class RenderController
{
    //прием action
    private $action;
    private $defaultAction = 'index';
    //шаблон по умолчанию
    private $layout = 'main';
    //поумолачнию будет true
    private $useLayout = true;

    //работа с action метод который будет вызвать нужный метод с класса
    public function runAction($action = null) {
        //если ничего не задано action станет index
        $this->action = $action ?: $this->defaultAction;
        $method = "action" . ucfirst($this->action);
        //у контроллера существует ли такой метод
        if (method_exists($this, $method)) {
            //если существует вызываем функцию эжтого метода
            $this->$method();
        } else {
            Die("Action не существует");
        }
    }

    //построение страницы
    public function render($template, $params = []) {

        //проверяем по умлочанию шаблон или нет
        if ($this->useLayout) {
            //по умолчанию рендерем шаблон с layouts и вторым параметром переменные
            return $this->renderTemplate("layouts/{$this->layout}", [
                'menu' => $this->renderTemplate('menu', $params),
                'content' => $this->renderTemplate($template, $params),
                'footer' => $this->renderTemplate('footer', $params),
            ]);
        } else {
            return $this->renderTemplate($template, $params = []);
        }
    }

    public function renderTemplate($template, $params = []) {
        //через буфер подключаем нужный шаблон
        ob_start();
        extract($params);
        //формируем путь для папки добавим имя шаблона и .php
        $templatePath = TEMPLATE_DIR . $template . ".php";
        if (file_exists($templatePath)) {
            include $templatePath;
            //вернем значение буфера и отчисим его
        }
        return  ob_get_clean();
    }

}