<?php


namespace app\controllers;

use app\model\DbModel;
use app\model\Quiz;
use app\model\Questions;
use app\model\Model;
use app\traits\TSingletone;
use mysql_xdevapi\Result;

class IndexController extends RenderController
{
    public function actionIndex()
    {
        echo $this->render('index', []);
    }
}