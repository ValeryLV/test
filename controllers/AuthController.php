<?php


namespace app\controllers;


use app\engine\Auth;

class AuthController extends RenderController
{
    public function actionIndex() {
        echo $this->render('errors/login');
    }

    public function actionLogin() {
        $login = $_POST['login'];
        $pass = $_POST['pass'];
        if (Auth::Auth($login, $pass)) {
            header("Location: /");
        } else {
            Die("Не верный логин пароль");
        }
    }

    public function actionLogout() {
        session_destroy();
        header("Location: /");
    }
}