<?php


namespace app\controllers;


use app\engine\Auth;
use app\model\Questions;
use app\model\Answers;
use app\model\Quiz;
use app\model\Results;
use app\model\Users;

class ResultController extends RenderController
{
    public function actionIndex()
    {
        if (!Auth::isAdmin() && !Auth::isModerator() && !Auth::isUser()) {
            echo $this->render('errors/login'); die();
        }

        //получение id вопроса
        $idAnswers = ($_SESSION['answers']);

        //переменные для подсчета правильных и не правильных ответов
        $no = 0;
        $yes = 0;

        //массив для записа ВОПРОСОВ на которые пользователь ответил не правильно
        $noQuestions = [];


        if (!is_null($idAnswers)) {

            //здесь будем делать аналог Group By по ответам
            $res = [];

            //Проходимся по всем ответам и группируем в res результаты
            foreach ($idAnswers as $idAnswer) {

                $answer = Answers::getOne($idAnswer);
                if (!$answer->is_true) {
                    $noQuestions[] = Questions::getOne($answer->question_id)->question;
                    $noQuestions = array_unique($noQuestions);
                }

                if (isset($res[$answer->question_id])) {
                    //Ответ верный, если нет ни одного false
                    $res[$answer->question_id] = $res[$answer->question_id] && $answer->is_true;
                } else {
                    $res[$answer->question_id] = (bool)$answer->is_true;
                }

            }

            //По res подсчитываем число верных - не верных ответов
            foreach ($res as $value) {
                if ($value) {
                    $yes++;
                } else {
                    $no++;
                }
            }

        }


        //Проверку на уникальность результатов
        if (!Results::getOneWhere('hash', $_SESSION['quizHash'])) {
            //Запись результатов
            $result = new Results(
                $_SESSION['errors']['login'] ?? 'guest',
                $_SESSION['currentQuizId'],
                $yes,
                $no,
                $_SESSION['quizHash'],
                $_SESSION['date']
            );

            $result->save();
        }

        echo $this->render('result', [
            'yes' => $yes,
            'no' => $no,
            'noQuestions' => $noQuestions,
        ]);
    }

    public function actionView()
    {

        $id_quiz = $_SESSION['errors']['quiz_id'];

        if (!Auth::isAdmin() && !(Auth::isModerator() && $id_quiz == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        //показываем список пользователей
        $users = Users::getAll();


        echo $this->render('admin/result/index', [
            'users' => $users
        ]);
    }

    public function actionOne()
    {
        $id_quiz = $_SESSION['errors']['quiz_id'];

        if (!Auth::isAdmin() && !(Auth::isModerator() && $id_quiz == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $id_user = (int)$_GET['id'];
        $user = Users::getOne($id_user);

        $results = Results::getResults($user->login);

        echo $this->render('admin/result/one', [
            'user' => $user->login,
            'results' => $results,
            'quizName' => $results[0]['name'],
            'attempt' => 1,
            'id_user' => $id_user,
        ]);
    }

    //CRUD
    public function actionDelete()
    {
        $id_quiz = $_SESSION['errors']['quiz_id'];

        if (!Auth::isAdmin() && !(Auth::isModerator() && $id_quiz == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $user = Results::getOne($_GET['id']);

        echo $this->render('admin/result/confirmDelete', [
            'title' => $user->login,
            'id' => $user->id,
            'id_user' => $_GET['id_user'],
        ]);
    }

    public function actionDeleteConfirmed()
    {
        $id = $_GET['id'];

        $user = Results::getOne($_GET['id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $user->quiz_id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }


        $status = $user->delete();

        $id_user = $_GET['id_user'];

        header("Location: /result/one/?id=" . $id_user);
    }
}