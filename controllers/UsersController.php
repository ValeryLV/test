<?php


namespace app\controllers;


use app\engine\Auth;
use app\model\Answers;
use app\model\Questions;
use app\model\Quiz;
use app\model\Users;
use http\Client\Curl\User;

class UsersController extends RenderController
{

    public function actionIndex()
    {
        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }
        $users = Users::getAll();

        echo $this->render('admin/users/index', [
            'user' => $users,
        ]);
    }

    //CRUD
    public function actionAdd()
    {
        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }
        $quiz = Quiz::getAll();

        echo $this->render('admin/users/add', [
            'quiz' => $quiz,
        ]);
    }

    //создание нового пользователя
    public function actionSave()
    {
        $user = new Users($_POST['login'], password_hash($_POST['pass'], PASSWORD_DEFAULT), $_POST['role'], $_POST['quiz_id']);

        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }
        $user->save();

        header("Location: /users/index");
    }

    //изменине пользователя рендер странцы для измений
    public function actionUpdate()
    {
        //получени ОБЪЕКТА по id в GET
        $user = Users::getOne($_GET['id']);
        $quiz = Quiz::getOne($user->quiz_id);
        $quizAll = Quiz::getAll();

        //проверка только на ADMIN = 1
        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        //рендер страцниы с переменными для внесения изменинй
        echo $this->render('admin/users/edit', [
            'id' => $user->id,
            'login' => $user->login,
            'role' => $user->role,
            'quiz_id' => $user->quiz_id,
            'quizName' => $quiz->name,
            'quizAll' => $quizAll,
        ]);
    }

    //само изменение пользователя
    public function actionStore()
    {
        $users = Users::getOne((int)$_POST['id']);

        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        //как сделать более удобно и красиво
        $users->login = $_POST['login'];
        $users->pass = password_hash($_POST['pass'], PASSWORD_DEFAULT);
        $users->role = $_POST['role'];
        $users->quiz_id = $_POST['quiz_id'];

        $users->save();

        header("Location: /users/index");
    }

    //удаления с подтверждением
    public function actionDelete()
    {
        $user = Users::getOne($_GET['id']);

        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/users/confirmDelete', [
            'id' => $user->id,
            'login' => $user->login,
            'pass' => $user->pass,
            'role' => $user->role,
            'quiz_id' => $user->quiz_id,
        ]);
    }

    //подтверждение удаления
    public function actionDeleteConfirmed()
    {
        $user = Users::getOne($_GET['id']);

        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        $status = $user->delete();
        // if ($status != 0) $_SESSION['success'] = true;

        header("Location: /users/index");
    }
}