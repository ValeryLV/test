<?php


namespace app\controllers;

use app\model\Questions;
use app\model\Quiz;
use app\engine\Auth;

class QuizController extends RenderController
{
    //получение викторин
    public function actionIndex()
    {
        $quiz = Quiz::getOne($_GET['id']);

        if (!Auth::isAdmin() && !Auth::isModerator() && !Auth::isUser()) {
            echo $this->render('errors/login'); die();
        }

        $quiz = Quiz::getAll();
        echo $this->render('quiz', [
            'quiz' => $quiz,
        ]);
    }

    //CRUD
    //показывает какую викторину можно редактирвоать
    public function actionEdit()
    {
        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        $quiz = Quiz::getAll();

        echo $this->render('admin/quiz/index', [
            'quiz' => $quiz,
        ]);
    }

    public function actionUpdate()
    {
        $quiz = Quiz::getOneWhere('slug', $_GET['custom_id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $questions = Questions::getAllWhere('quiz_id', $quiz->id);

        echo $this->render('admin/quiz/edit', [
            'title' => $quiz->name,
            'info' => $quiz->info,
            'slug' => $quiz->slug,
            'id' => $quiz->id,
            'questions' => $questions
        ]);
    }

    public function actionAdd()
    {
        if (!Auth::isAdmin())  {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/quiz/add');
    }

    public function actionSave()
    {

        if (!Auth::isAdmin()) {
            echo $this->render('errors/login'); die();
        }

        (new Quiz($_POST['title'], $_POST['info'], $_POST['slug']))->save();

        header("Location: /quiz/edit");
    }

    public function actionStore()
    {
        $quiz = Quiz::getOne($_POST['id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $quiz->name = $_POST['title'];
        $quiz->info = $_POST['info'];
        $quiz->slug = $_POST['slug'];
        $quiz->save();

        //header("Location: " . $_SERVER['HTTP_REFERER']);
        header("Location: /quiz/update/" . $_POST['slug']);
    }

    public function actionDelete()
    {
        $quiz = Quiz::getOneWhere('slug', $_GET['custom_id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        echo $this->render('admin/quiz/confirmDelete', [
            'title' => $quiz->name,
            'id' => $quiz->id
        ]);
    }

    public function actionDeleteConfirmed()
    {
        $quiz = Quiz::getOne($_GET['custom_id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $status = $quiz->delete();

        header("Location: /quiz/edit");
    }

}