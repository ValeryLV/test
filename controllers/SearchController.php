<?php


namespace app\controllers;


use app\engine\Auth;
use app\model\Answers;
use app\model\Questions;
use app\model\Quiz;

class SearchController extends RenderController
{
    public function actionQuestions()
    {
        $quiz = Quiz::getOneWhere('slug', $_GET['custom_id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $question = $_POST['question'];

        $questions = Questions::getAllWhere('question', $question, 'like', $quiz->id);

        echo $this->render('admin/quiz/edit', [
            'title' => $quiz->name,
            'info' => $quiz->info,
            'id' => $quiz->id,
            'slug' => $quiz->slug,
            'questions' => $questions,
            'searchText' => strip_tags(htmlspecialchars($question)),
        ]);
    }

    public function actionAnswers()
    {
        $quiz = Quiz::getOneWhere('slug', $_GET['custom_id']);

        if (!Auth::isAdmin() && !(Auth::isModerator() && $quiz->id == Auth::getModeratorIdQuiz())) {
            echo $this->render('errors/login'); die();
        }

        $answer = $_POST['answer'];

        $answers = Answers::getAllWhere('text', $answer, 'join', $quiz->id);

        foreach ($answers as $value)
        {
            if ($value->question_id == $lastQuestion)
            {
                $value->question = '';
            }
            $lastQuestion = $value->question_id;
        }

        echo $this->render('admin/questions/edit', [
            'answers' => $answers,
            'searchText' => strip_tags(htmlspecialchars($answer)),
            'id' => $quiz->id,
            'slug' => $quiz->slug,
        ]);
    }
}