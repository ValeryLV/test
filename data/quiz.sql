-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 27 2021 г., 00:44
-- Версия сервера: 8.0.19
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `quiz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answer`
--

CREATE TABLE `answer` (
  `id` int NOT NULL,
  `text` text COLLATE utf8mb4_general_ci NOT NULL,
  `question_id` int NOT NULL,
  `is_true` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `answer`
--

INSERT INTO `answer` (`id`, `text`, `question_id`, `is_true`) VALUES
(221, '0', 90, 1),
(222, '16', 90, 0),
(223, '103', 90, 0),
(224, '12', 90, 0),
(225, 'Трёхлистный клевер', 91, 0),
(226, 'Три лепестка сакуры', 91, 0),
(227, 'Три водяных лилии', 91, 0),
(228, 'Три водяных ореха', 91, 1),
(229, 'Здороваются с водителями соседних машин', 92, 0),
(230, 'Ставят машину на \"ручнки\"', 92, 0),
(231, 'Глушат двигатель', 92, 0),
(232, 'Выключают фары', 92, 1),
(233, 'Диана', 93, 0),
(234, 'Флора', 93, 0),
(235, 'Терписхора', 93, 0),
(236, 'Фауна', 93, 1),
(237, '5 лет', 94, 0),
(238, '1 месяц', 94, 0),
(239, '1 год', 94, 0),
(240, '6 месяцев', 94, 1),
(241, 'СССР', 95, 0),
(242, 'Камерун', 95, 1),
(243, 'ЮАР', 95, 0),
(244, 'Германия', 95, 0),
(245, 'Матовые диски', 96, 0),
(246, 'Спойлер', 96, 0),
(247, 'Автосигнализация', 96, 0),
(248, 'Тонировка', 96, 1),
(249, 'Риге', 97, 0),
(250, 'Москве', 97, 0),
(251, 'Таллинне', 97, 0),
(253, 'Ленинграде', 97, 1),
(254, 'Знака аварийной остановке', 98, 0),
(255, 'Пакета документов', 98, 0),
(256, 'Яркого жилета', 98, 1),
(257, 'Фонарика', 98, 0),
(258, 'Собственная служба безопасности ', 99, 1),
(259, 'Нефтяные лоббисты', 99, 0),
(260, 'Конгресс США', 99, 0),
(261, 'Жена', 99, 0),
(262, 'Автобокс на крышу', 100, 0),
(263, 'Прицеп-дача', 100, 1),
(264, 'Алюминиевый обвес', 100, 0),
(265, 'Кондиционер', 100, 0),
(266, 'str_repeat()', 101, 0),
(267, 'strtok()', 101, 0),
(268, 'strpos()', 101, 1),
(269, 'substr()', 101, 0),
(270, 'Василий что-то сломал в настройках sendmail.', 102, 0),
(271, 'Всё должно доходить, а ошибка в коде.', 102, 0),
(272, 'Denwer по умолчанию использует свой sendmail, который лишь эмулирует отправку, но в реальности ничего не отправляет.', 102, 1),
(273, 'Mail.ru и другие серьёзные почтовые серверы не принимают письма, отправленные с локального сервера.', 102, 0),
(274, 'document.location = “http://google.ru”;', 103, 0),
(275, 'header(“Location: http://google.ru”);', 103, 1),
(276, 'location.href = “http://google.ru”;', 103, 0),
(277, 'header(“Redirect: http://google.ru”);', 103, 0),
(278, '13', 104, 1),
(279, '024', 104, 0),
(280, '013', 104, 0),
(281, '24', 104, 0),
(282, 'Функция strpos() вернула 0, который в PHP равен false. Чтобы не было ошибки надо вместо знака равенстка (==) использовать знак эквивалентности (===).', 105, 1),
(283, 'Нет фигурных скобок у блока операторов при срабатывании условия.', 105, 0),
(284, 'Функция strpos() неправильно использована. Сначала должна идти искомая строка, а уже потом та строка, в которой происходит поиск.', 105, 0),
(285, 'Здесь нет ошибок, так как strpos(“mystring”, “m”) не равен false, и строки “Символа m в строке mystring нет” не появляется.', 105, 0),
(286, 'Функции session_start() не существует, правильная функция – start_session().', 106, 0),
(287, 'Нельзя выводить информацию до начала сессии.', 106, 1),
(288, 'После начала сессии необходимо её использовать, а в коде она никак не используется.', 106, 0),
(289, 'В этом коде нет ошибок.', 106, 0),
(291, 'Никакой разницы нет.', 107, 0),
(292, 'Знак эквивалентности проверяет лишь значения операндов, а знак равенства значения и их типы.', 107, 0),
(293, 'Знак эквивалентности работает лишь для строк, а знак равенства применим к любым типам.', 107, 0),
(294, 'Знак равенства проверяет лишь значения операндов, а знак эквивалентности значения и их типы.', 107, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int NOT NULL,
  `question` text COLLATE utf8mb4_general_ci NOT NULL,
  `quiz_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `question`, `quiz_id`) VALUES
(90, 'В 1909 году кардинал Фарлен подарил папе римскому Пию Х лимузин Itala 20/30. Сколько км проездил Пий Х на первом \"папамобиле\"?', 51),
(91, 'Как переводится название японской компании \"Mutsubishi\"?', 51),
(92, 'Что делают все японский водители, когда останавливаются на перекрестках??', 51),
(93, 'Как назывался гаражный кооператив в комедии Эльдара Рязанова \"ГАРАЖ\"?', 51),
(94, 'Сколько времени, согласно исследования, тратят жители крупных городов на ожидание зеленного сигнала светафора?', 51),
(95, 'Спортсмены из какой страны выступали в шоссейной гонке Московской олимпиады-80 на выпущенных специально к этим соревнованиям советских велосипедах \"Москва-80\"?', 51),
(96, 'Какой элемент для всех автомобилей в штате Калифорния США с 2012 года является обязательным?', 51),
(97, 'В каком городе в 1930 году был установлен первый в СССР светофор?', 51),
(98, 'Без чего нельзя выходить из машины на дорогах вне населенных пунктов в странах Евросоюза?', 51),
(99, 'Кто помешал президенту США Бараку Обаме установить на свой президентский автомобильный гибридный двигатель?', 51),
(100, 'Какой необычный для СССР \"АКСЕССУАР\" к автомобилю \"ПОБЕДА\" был выпущен в Риге 1958 году?', 51),
(101, 'Какая функция в PHP позволяет найти вхождение подстроки?', 52),
(102, 'Василий установил пакет Denwer и написал скрипт, который должен отправлять письма на электронный ящик mail.ru. Но письма почему-то не доходили. Почему?', 52),
(103, 'Как сделать редирект (например, на google.ru) на PHP?', 52),
(104, 'Настя написала такой код : \"for ($i = 0; $i < 5; $i++) { if ($i % 2 == 0) continue; echo $i; }\" Что она увидит после запуска скрипта?', 52),
(105, 'Есть строка: “my string”. Есть код: \"if (strpos(\"mystring\", \"m\") == false) echo \"Символа m в строке mystring нет\";\"     В чём заключается ошибка в коде?', 52),
(106, 'Константин написал такой код: \"echo \"Привет\"; session_start();\" Но при попытке его выполнить у него возникла ошибка. С чем она связана?', 52),
(107, 'Какая разница между равенством и эквивалентностью?', 52);

-- --------------------------------------------------------

--
-- Структура таблицы `quiz`
--

CREATE TABLE `quiz` (
  `id` int NOT NULL,
  `name` text COLLATE utf8mb4_general_ci NOT NULL,
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `quiz`
--

INSERT INTO `quiz` (`id`, `name`, `info`, `slug`) VALUES
(51, 'Тест: Что ты знаешь из истории автомобилей?', 'В начале истории автомобилестроения первые самодвижущиеся экипажи мало отличались от конных и лишь ненамного обгоняли их. Но с первых лет появления транспортных средств с двигателями внутреннего сгорания энтузиасты начали совершенствовать их, находить новые решения в конструкции и, соответственно, конкурировать между собой.', 'auto'),
(52, 'Тест: PHP. Уверен что сможешь пройти?', 'PHP онлайн тесты оценивают знания кандидатов программирования на языке PHP и их возможности использовать широко используемые в этом языке возможности. Тест по PHP — это идеальная оценка для скрининга до работы. ... Также PHP тестирование поможет оценить реальный уровень знаний программиста.', 'php');

-- --------------------------------------------------------

--
-- Структура таблицы `results`
--

CREATE TABLE `results` (
  `id` int NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `quiz_id` int NOT NULL,
  `yes` int NOT NULL,
  `no` int NOT NULL,
  `hash` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `results`
--

INSERT INTO `results` (`id`, `login`, `quiz_id`, `yes`, `no`, `hash`, `created_at`) VALUES
(150, 'redactor', 40, 1, 2, '400025505600803cce1ce49.27578622', '2021-01-20 01:19:56'),
(151, 'Admin', 24, 3, 2, '1566790774600e9b7f1dc6c5.35738379', '2021-01-25 01:20:47'),
(152, 'Admin', 23, 0, 5, '429324600e9ed5a98455.78354410', '2021-01-25 01:35:01'),
(153, 'Admin', 23, 1, 4, '307308925600ea013493fa1.77105476', '2021-01-25 01:40:19'),
(154, 'Admin', 24, 0, 5, '164388845600ea6a2af0090.20735637', '2021-01-25 02:08:18'),
(155, 'Admin', 23, 0, 5, '494850561600ea9d7801e00.38443209', '2021-01-25 02:21:59'),
(156, 'Admin', 23, 0, 5, '1212461277600ea9eabdfec9.61062268', '2021-01-25 02:22:18'),
(157, 'Admin', 23, 0, 5, '340523572600eaa038fcdf2.89821794', '2021-01-25 02:22:43'),
(158, 'Admin', 23, 0, 5, '1852908964600eab3095bd47.54781854', '2021-01-25 02:27:44'),
(159, 'Admin', 23, 0, 5, '1747407584600eac14a12a22.31465435', '2021-01-25 02:31:32'),
(160, 'Admin', 23, 5, 0, '783889063600eacd4898f23.78825144', '2021-01-25 02:34:44'),
(161, 'Admin', 23, 3, 2, '110256323600eb625134701.87055003', '2021-01-25 03:14:29'),
(162, 'Admin', 23, 3, 2, '394775807600eb641a667f8.67430665', '2021-01-25 03:14:57'),
(163, 'Admin', 23, 1, 4, '1415183805600eb92e61b7e4.64329263', '2021-01-25 03:27:26'),
(164, 'Admin', 24, 1, 4, '2002124433600ebb3ff0f583.04830810', '2021-01-25 03:36:15'),
(165, 'Admin', 24, 1, 4, '213978200600ebb9def9674.85364495', '2021-01-25 03:37:49'),
(166, 'Admin', 24, 0, 5, '1344810049600ebddf520e98.61748832', '2021-01-25 03:47:27'),
(167, 'Admin', 24, 3, 2, '1984339091600ebe35a9f724.13801172', '2021-01-25 03:48:53'),
(168, 'redactor', 23, 4, 1, '1142635023600ece1bc8b7c0.77587059', '2021-01-25 04:56:43'),
(169, 'Admin', 23, 5, 0, '1763072586600ece8aea8b53.18664303', '2021-01-25 04:58:34'),
(170, 'Admin', 37, 0, 0, '1173987936600ece9e567649.94454487', '2021-01-25 04:58:54'),
(171, 'Admin', 23, 3, 2, '1862614288600ed16f692a87.13253568', '2021-01-25 05:10:55'),
(172, 'Admin', 37, 0, 0, '2057977664600ed17fa8c0d5.59988475', '2021-01-25 05:11:11'),
(173, 'Admin', 24, 2, 3, '45430062600ed2a43fbe28.96804212', '2021-01-25 05:16:04'),
(174, 'Admin', 23, 3, 2, '1267051456600ed534b5cf93.61132734', '2021-01-25 05:27:00'),
(175, 'Admin', 24, 1, 4, '1696995088600ed5926a7bf5.93711598', '2021-01-25 05:28:34'),
(176, 'Admin', 23, 1, 4, '1925644537600ed5a3965b20.19559552', '2021-01-25 05:28:51'),
(177, '123', 23, 2, 3, '1976874420600edf699bee82.44302957', '2021-01-25 06:10:33'),
(178, 'Admin', 23, 2, 3, '919540222600ee332b032a1.71608374', '2021-01-25 06:26:42'),
(179, 'user', 48, 2, 1, '1681655806600ff7bc385770.80029492', '2021-01-26 02:06:36'),
(181, 'Admin', 48, 2, 1, '133231666560100dedd94ce8.60390769', '2021-01-26 03:41:17'),
(183, 'Admin', 48, 3, 0, '74534838160100e2b93c7c1.01316084', '2021-01-26 03:42:19'),
(194, 'Admin', 51, 9, 2, '4747943736010647e71bd35.03334149', '2021-01-26 09:50:38'),
(195, 'admin', 51, 0, 11, '154840323601064dfac7114.56437803', '2021-01-26 09:52:15'),
(196, 'admin', 52, 0, 7, '1701570066601068285c0791.56550450', '2021-01-26 10:06:16'),
(197, 'admin', 52, 7, 0, '20852515566010684b902149.08029057', '2021-01-26 10:06:51');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `role` int NOT NULL DEFAULT '0',
  `quiz_id` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `pass`, `role`, `quiz_id`) VALUES
(29, 'admin', '$2y$10$rAHGa/fW40dqRsyV5OBGDehVwV5qkLBYyIO6CPDkE8K6QINK.WTAG', 1, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_ibfk_1` (`quiz_id`);

--
-- Индексы таблицы `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `quiz` ADD FULLTEXT KEY `slug` (`slug`);

--
-- Индексы таблицы `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT для таблицы `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT для таблицы `results`
--
ALTER TABLE `results`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
