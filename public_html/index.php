<?php
session_start();
//date_default_timezone_set('Europe/Moscow');
include "../config/config.php";

use app\engine\Autoload;

//подключение автозагрузчка
include "../engine/Autoload.php";

//спец обертка автозагрузчика для нескольких автозагрузчиков, можно передать массив экземпляра класса котрый реализует автозагрузку а вторым метод который осуще автозагрузку
spl_autoload_register([new Autoload(), 'loadClass']);

$url = explode('/', $_SERVER['REQUEST_URI']);

$controllerName = $url[1] ?: 'index';
$actionName = $url[2];
$_GET['custom_id'] = $url[3];

//собираем имя класса контрорллера которое нужно создать с большой буквы с подключене пространства имен
$controllerClass = CONTROLLER_NAMESPACE . ucfirst($controllerName) . "Controller";

//проверяет на несуществующий контроллер
if (class_exists($controllerClass)) {
    //если есть контроллер будет создан класс с его именим
    $controller = new $controllerClass();
    //запуск action что хочет пользователь
    $controller->runAction($actionName);
} else {
    Die("Контроллер не существует!");
}
//$2y$10$gUzjeTd7Tionc.qxxgfUfe8y7gi0/MxNB5hANOgOC638AFdKuBM9a   =   123