<div class="quiz">
    <?php foreach ($quiz as $item): ?>
        <div class="card" style="width: 18rem; margin: 20px;">
            <div class="card-body">
                <h5 class="card-title"><?= $item->name?></h5>
                <p class="card-text"><?= $item->info?></p>
                <a href="/questions/one/?id=<?= $item->id ?>&page=0" class="btn btn-primary">Начать</a>
            </div>
        </div>
    <? endforeach; ?>
</div>