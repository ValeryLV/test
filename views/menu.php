<?php use app\engine\Auth; ?>
<ul class="nav nav-pills menu">
    <li class="nav-item">
        <a href="/" class="btn nav-link active">Онлайн Тесты</a>
    </li>
        <?php if (Auth::isAdmin()) : ?>
            <li class="nav-item">
                <a class="nav-link btn btn-danger" href="/quiz">Выбор дисциплины</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['errors']['login'] ?></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/quiz/edit">Редактирование</a>
                    <a class="dropdown-item" href="/auth/logout">Выйти</a>
                </div>
            </li>
        <?php elseif (Auth::isModerator()) : ?>
            <li class="nav-item">
                <a class="nav-link btn btn-danger" href="/quiz">Выбор дисциплины</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['errors']['login'] ?></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/quiz/update/<?= $_SESSION['errors']['slug'] ?>">Правка викторин</a>
                    <a class="dropdown-item" href="/result/view">Просмотр результатов</a>
                    <a class="dropdown-item" href="/auth/logout">Выйти</a>
                </div>
            </li>
        <?php elseif (Auth::isUser()) : ?>
            <li class="nav-item">
                <a class="nav-link btn btn-danger" href="/quiz">Выбор дисциплины</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['errors']['login'] ?></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/auth/logout">Выйти</a>
                </div>
            </li>
        <?php else : ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Личный кабинет</a>
                <form class="dropdown-menu" action="/auth/login" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmailAccount">Ваш логин</label>
                        <input id="exampleInputEmailAccount" class="form-control" type="text" aria-describedby="emailHelp" placeholder="Enter login" name="login" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPasswordAccount">Ваш пароль</label>
                        <input id="exampleInputPasswordAccount" type="password" class="form-control" placeholder="Password" name="pass" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Войти</button>
                </form>
            </li>
        <?php endif; ?>
</ul>
