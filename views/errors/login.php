<div class="admin card-border">
    <h3>У Вас не хватает прав. Зайдите под администратором.</h3>
    <form action="/auth/login" method="post">
        <div class="form-group">
            <label for="exampleInputEmailAdmin">Администратор</label>
            <input id="exampleInputEmailAdmin" class="form-control" type="text" aria-describedby="emailHelp" placeholder="Enter login" name="login" required>
            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPasswordAdmin">Пароль</label>
            <input id="exampleInputPasswordAdmin" type="password" class="form-control" placeholder="Password" name="pass" required>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>
</div>
