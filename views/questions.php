<div class="question card-border">
    <p><strong>Вопрос - <?= $count ?> из <?= $countAll ?></strong></p>
    <form action="<?php $_SERVER['REQUEST_URI'] ?>" method="POST">
        <p class="lead"><?= $questions->question ?></p>
        <?php foreach ($answers as $answer): ?>
        <div class="answer">
            <div class="form-check">
                <input class="input-answer form-check-input" type="checkbox" name="<?= $answer->id ?>" id="<?= $answer->id ?>">
                <label style="white-space: inherit;" class="form-check-label btn btn-primary" for="<?= $answer->id ?>">
                    <?= $answer->text ?>
                </label>
            </div>
        </div>
        <?php endforeach; ?>
        <button class="btn btn-danger" type="submit" name="ok">Ответить
        </button>
    </form>
</div>