<div class="blockquote result card-border">
    <p class="mb-0"><strong>Результат теста</strong></p>
    <blockquote class="blockquote text-center">
        <p class="mb-0">Правильных ответов: <strong><?= $yes ?></strong></p>
    </blockquote>
    <blockquote class="blockquote text-center">
        <p class="mb-0">Не правильных ответов: <strong><?= $no ?></strong></p>
    </blockquote>
    <blockquote class="blockquote">
        <p class="mb-0"><strong>Вопросы на которые ответили не правильно:</strong></p>
        <?php foreach ($noQuestions as $noQuestion) : ?>
            <p class="lead">
                - <?= $noQuestion ?>
            </p>
        <?php endforeach; ?>
    </blockquote>
</div>