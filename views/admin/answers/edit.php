<div class="admin card-border">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/quiz/update/<?=$slug?>">Редактор</a></li>
            <li class="breadcrumb-item"><a href="/questions/update/?id=<?=$question_id?>">Вопрос</a></li>
            <li class="breadcrumb-item active" aria-current="page"">Ответ</li>
        </ol>
    </nav>
    <form action="/answer/store" method="post">
        <input hidden type="text" name="id" value="<?=$id?>">
        <input hidden type="text" name="questions_id" value="<?=$question_id?>">
        <div class="form-group">
            <label for="exampleInputAnswerName">Изменить ответ:</label>
            <div class="answer-checkbox__left_block">
                <input <? if ($is_true == '1') echo 'checked'?>
                        class="answer-checkbox"
                        id="<?= $id ?>"
                        name="is_true"
                        type="checkbox"
                        value="1">
                <input type="text" name="text" required class="form-control answer-checkbox__left text-space" id="exampleInputAnswerName" aria-describedby="emailHelp" value="<?=$text?>" placeholder="<?=$text?>">
            </div>
        </div>
        <button type="submit" class="btn btn-success">Изменить</button>
    </form>
</div>