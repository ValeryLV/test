<div class="admin">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/result/view">Результаты</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?=$user?></li>
        </ol>
    </nav>
<p class="h5">Результаты: <span class="yellow">"<?=$user?>"</span></p>
    <?php if (!empty($results)): ?>
    <p class="h5">Викторина: <span class="yellow">"<?=$quizName?>"</span></p>
        <table class="table block-center">
            <thead>
            <tr class="table-info">
                <th scope="col">N п/п</th>
                <th scope="col">Дата</th>
                <th scope="col">Правильных ответов:</th>
                <th scope="col">Не правильных ответов:</th>
                <th scope="col">Примичание</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $result): ?>
                <tr>
                    <th scope="row"><?=$attempt++?></th>
                    <td><?= date('d.m.y', strtotime($result['created_at']))?></td>
                    <td><?=$result['yes']?></td>
                    <td><?=$result['no']?></td>
                    <td><a href="/result/delete/?id=<?=$result['id']?>&id_user=<?=$id_user?>">Удалить</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
    <p>Данный пользователь тесты еще не проходил.</p>
    <?php endif ?>
</div>