<div class="admin">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Результаты</li>
        </ol>
    </nav>
    <?php foreach ($users as $user): ?>
        <div class="alert alert-primary" role="alert">
            <a class="alert-link" href="/result/one/?id=<?=$user->id?>"><?=$user->login?></a>
        </div>
    <?php endforeach; ?>
</div>
