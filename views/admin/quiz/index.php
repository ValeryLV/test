<div class="container-fluid">
    <div class="quiz">
        <?php foreach ($quiz as $item): ?>
            <div class="card" style="width: 18rem; margin: 20px;">
                <div class="card-body">
                    <h5 class="card-title"><?= $item->name?></h5>
                    <a class="btn btn-warning" href="/quiz/update/<?= $item->slug?>">Редактировать</a>
                    <a class="btn btn-danger" href="/quiz/delete/<?= $item->slug?>">Удалить</a>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <div class="center__blocks">
        <a class="btn btn-success" href="/quiz/add">Добавить викторину</a>
        <a class="btn btn-info" href="/users/index">Редактор админов</a>
    </div>
</div>