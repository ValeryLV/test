<div class="admin card-border">
    <form action="/quiz/save" method="post">
        <div class="form-group">
            <label for="exampleInputQuizName">Напишите название викторины</label>
            <input type="text" name="title" required class="form-control" id="exampleInputQuizName" aria-describedby="emailHelp" placeholder="Название виктирины">
        </div>
        <div class="form-group">
            <label for="exampleInputQuizInfo">Напишите краткое описание викторины</label>
            <input type="text" name="info" required class="form-control" id="exampleInputQuizInfo" placeholder="Описание викторины">
        </div>
        <div class="form-group">
            <label for="exampleInputQuizURL">Напишите адресс страницы "URL"</label>
            <input type="text" name="slug" required class="form-control" id="exampleInputQuizURL" placeholder="URL">
            <small id="emailHelp" class="form-text text-muted">Пример "http://quiz/<em><u>history</u></em>"</small>
        </div>
        <button type="submit" class="btn btn-success">Добавить</button>
    </form>
</div>

