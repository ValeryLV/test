<div class="admin card-border">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактор</li>
        </ol>
    </nav>
    <form action="/quiz/store/?id=<?=$id?>" method="post">
        <div class="form-group col-md-6">
            <label for="inputQuizName">Название викторины:</label>
            <input type="text" name="title" required class="form-control" id="inputQuizName" value="<?=$title?>" placeholder="<?=$title?>">
        </div>
        <div class="form-group col-md-6">
            <label for="inputQuizInfo">Описание викторины:</label>
            <input type="text" name="info" required class="form-control" id="inputQuizInfo" value="<?=$info?>" placeholder="<?=$info?>">
        </div>
        <div class="form-group col-md-6">
            <label for="inputQuizURL">URL</label>
            <input type="text" name="slug" required class="form-control" id="inputQuizURL" value="<?=$slug?>" placeholder="<?=$slug?>">
            <small id="emailHelp" class="form-text text-muted">Пример "http://quiz/<em><u>history</u></em>"</small>
        </div>
        <input hidden type="text" name="id" value="<?=$id?>">
        <input type="submit" class="btn btn-success" value="Изменить">
    </form>

    <div class="admin__search">
        <form action="/search/questions/<?=$slug?>" method="post">
            <label class="sr-only" for="inlineFormInputGroupQuestions">Поиск по вопросу:</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
                </div>
                <input type="text" name="question" class="form-control" id="inlineFormInputGroupQuestions" placeholder="Напишите вопрос . . .">
            </div>
        </form>
        <form class="" action="/search/answers/<?=$slug?>" method="post">
            <label class="sr-only" for="inlineFormInputGroupAnswers">Поиск по вопросу:</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
                </div>
                <input type="text" name="answer" class="form-control" id="inlineFormInputGroupAnswers" placeholder="Напишите ответ . . .">
            </div>
        </form>
    </div>

    <? if (!empty($searchText)): ?>
        <h4>Результаты поиска по запросу <span class="yellow"><?= $searchText ?></span></h4>
    <?php if (!empty($questions)): ?>
        <h6>Редактирвоание вопросов викторины:</h6>
        <?php foreach ($questions as $item): ?>
            <div class="alert alert-primary admin__questions-edit">
                <p class="h5 text-space width-rem"><?= $item->question?></p>
                <div>
                    <a class="btn btn-warning" href="/questions/update/?id=<?= $item->id ?>">Изменить</a>
                    <a class="btn btn-danger" href="/questions/delete/?id=<?=$item->id?>">удалить</a>
                </div>
            </div>
            <? endforeach; ?>
            <a class="btn btn-success" href="/questions/add/?id=<?=$id?>">Добавить вопрос:</a>
        <? else: ?>
            <h6>Вопросов нет.</h6>
            <a class="btn btn-success" href="/questions/add/?id=<?=$id?>">Добавить вопрос:</a>
        <? endif ?>
    <? else: ?>
    <? if (!empty($questions)): ?>
        <h6>Редактирвоание вопросов викторины:</h6>
        <?php foreach ($questions as $item): ?>
            <div class="alert alert-primary admin__questions-edit">
                <p class="h5 text-space width-rem"><?= $item->question?></p>
                <div>
                    <a class="btn btn-warning" href="/questions/update/?id=<?= $item->id ?>">Изменить</a>
                    <a class="btn btn-danger" href="/questions/delete/?id=<?=$item->id?>">удалить</a>
                </div>
            </div>
        <? endforeach; ?>
            <a class="btn btn-success" href="/questions/add/?id=<?=$id?>">Добавить вопрос:</a>
    <? else: ?>
    <h6>Вопросов нет.</h6>
    <a class="btn btn-success" href="/questions/add/?id=<?=$id?>">Добавить вопрос:</a>
    <? endif ?>
    <? endif ?>
</div>