<div class="admin card-border">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/quiz/update/<?=$slug?>">Редактор</a></li>
            <li class="breadcrumb-item active" aria-current="page"">Вопрос</li>
        </ol>
    </nav>
    <? if (empty($searchText)): ?>
        <p>Редактирование вопроса:</p>
        <form action="/questions/store" method="post">
            <div class="form-group col-md-6">
                <label for="inputQuestionName">Название вопроса:</label>
                <input type="text" name="title" required class="form-control" id="inputQuestionName" value="<?=$title?>" placeholder="<?=$title?>">
            </div>
            <input hidden type="text" name="id" value="<?=$id?>">
            <input hidden type="text" name="quiz_id" value="<?=$quiz_id?>">
            <input type="submit" class="btn btn-success" value="Изменить">
        </form>
    <?endif; ?>

    <? if (!empty($searchText)): ?>
        <h4>Результаты поиска по запросу <span class="yellow"><?= $searchText ?></span></h4>
    <?php if (!empty($answers)): ?>
        <h6>Редактирование ответов:</h6>
        <?php foreach ($answers as $answer): ?>
                <p class="h5 text-space"><?= $answer->question ?></p>
                <div class="alert alert-primary admin__questions-edit">
                    <input <? if ($answer->is_true) echo 'checked'?>
                            class="answer-checkbox"
                            id="<?= $answer->id ?>"
                            name="<?= $answer->id ?>"
                            disabled
                            type="checkbox">
                    <p class="answer-checkbox__center text-space">
                        <?= $answer->text ?>
                    </p>
                    <div>
                        <a class="btn btn-warning" href="/answer/update/?id=<?= $answer->id ?>">Изменить</a>&nbsp;
                        <a class="btn btn-danger" href="/answer/delete/?id=<?=$answer->id?>">Удалить</a>&nbsp;
                    </div>
                </div>
        <?php endforeach; ?>
        <?php else: ?>
            <p>Вопросов нет.</p>
            <form action="/search/answers/<?=$slug?>" method="post">
                <label class="sr-only" for="inlineFormInputGroupAnswers">Поиск по вопросу:</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
                    </div>
                    <input type="text" name="answer" class="form-control" id="inlineFormInputGroupAnswers" placeholder="Напишите ответ . . .">
                </div>
            </form>
        <?php endif ?>
    <?php else: ?>
        <?php if (!empty($answers)): ?>
            <p>Редактирование ответов:</p>
            <?php foreach ($answers as $answer): ?>
                <div class="alert alert-primary admin__questions-edit">
                        <input <? if ($answer->is_true) echo 'checked'?>
                                class="answer-checkbox"
                                id="<?= $answer->id ?>"
                                name="<?= $answer->id ?>"
                                disabled
                                type="checkbox">
                    <p class="answer-checkbox__center text-space">
                        <?= $answer->text ?>
                    </p>
                        <div>
                            <a class="btn btn-warning" href="/answer/update/?id=<?= $answer->id ?>">Изменить</a>&nbsp;
                            <a class="btn btn-danger" href="/answer/delete/?id=<?=$answer->id?>">Удалить</a>&nbsp;
                        </div>
                </div>
            <?php endforeach; ?>
            <form action="/answer/save" method="post">
                <div class="form-group col-md-6">
                    <label for="inputQuestionAdd">Добавление ответа:</label>
                    <div>
                        <input type="checkbox" name="is_true" value="1" class="answer-checkbox">
                        <input type="text" name="title" required class=" answer-checkbox__left form-control" id="inputQuestionAdd" value="" placeholder="Напишите ответ . . . ">
                    </div>
                </div>
                <input hidden type="text" name="questions_id" value="<?=$id?>">
                <input class="btn btn-success" type="submit" value="Добавить">
            </form>
        </div>
        <?php else: ?>
            <p>Ответов нет.</p>
            <form class="" action="/answer/save" method="post">
                <div class="form-group col-md-6">
                    <label for="inputQuestionAdd">Добавление ответа:</label>
                    <div>
                        <input type="checkbox" name="is_true" value="1" class="answer-checkbox">
                        <input type="text" name="title" required class=" answer-checkbox__left form-control" id="inputQuestionAdd" value="" placeholder="Напишите ответ . . . ">
                    </div>
                </div>
                <input hidden type="text" name="questions_id" value="<?=$id?>">
                <input class="btn btn-success" type="submit" value="Добавить">
            </form>
        <? endif; ?>
<? endif; ?>



