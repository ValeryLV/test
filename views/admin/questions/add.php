<div class="admin card-border">
    <form action="/questions/save" method="post">
        <input hidden name="quiz_id" value="<?= $quiz_id ?>">
        <div class="form-group">
            <label for="inputAnswerAdd">Напишите вопрос:</label>
            <input type="text" name="question" required class="form-control" id="inputAnswerAdd" aria-describedby="emailHelp" placeholder="Название вопроса">
        </div>
        <button type="submit" class="btn btn-success">Добавить</button>
    </form>
</div>