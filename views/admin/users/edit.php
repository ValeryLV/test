<div class="admin card-border">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/users/index">Редактор пользователей</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?=$login?></li>
        </ol>
    </nav>
<form action="/users/store" method="post">
    <div class="form-row">
        <input hidden type="text" name="id" value="<?=$id?>">
        <div class="form-group col-md-6">
            <label for="inputEmailUser">Изменить</label>
            <input id="inputEmailUser" type="text" name="login" required class="form-control" value="<?=$login?>" placeholder="<?=$login?>">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPasswordUser">Создайте пароль для пользователя</label>
            <input id="inputPasswordUser" type="password" name="pass" required class="form-control" placeholder="Password">
        </div>
        <div class="form-group col-md-4">
            <label for="levelAccess">Уровень доступа</label>
            <select id="levelAccess" class="form-control" name="role">
                <option
                    <? if ($role == 1) echo 'selected'?>
                        class="alert-danger" value="1">Уровень доступа - 1</option>
                <option
                    <? if ($role == 2) echo 'selected'?>
                        class="alert-warning" value="2">Уровень доступа - 2</option>
                <option
                    <? if ($role == 3) echo 'selected'?>
                        class="alert-primary" value="3">Уровень доступа - 3</option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="editQuiz">Редактирвоание викторины</label>
            <select  id="editQuiz" class="form-control" name="quiz_id">
                <option
                        selected><?=$quizName?></option>
                <? foreach ($quizAll as $item): ?>
                    <option
                            value="<?=$item->id?>"><?=$item->name?>
                    </option>
                <? endforeach; ?>
                <option value="0">Не редактирует викторину</option>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Изменить</button>
</form>
</div>


