<div class="admin">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb alert-info">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактор пользователей</a></li>
        </ol>
    </nav>
    <?php foreach ($user as $item): ?>
    <div class="alert alert-primary admin__users">
        <p class="h3"><?= $item->login?></p>
        <div>
            <a class="btn btn-warning" href="/users/update/?id=<?= $item->id?>">Изменить</a>
            <a class="btn btn-danger" href="/users/delete/?id=<?= $item->id?>">Удалить</a>
        </div>
    </div>
    <? endforeach; ?>
    <a class="btn btn-success" href="/users/add/">Добавить админа</a>
</div>

