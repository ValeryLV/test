<?php

namespace app\interfaces;

interface IModel
{
    //статичный метод чтобы не создавать экзмепляры классов и возвращать объект для дальнейшей работы
    public static function getOne($id);
    public static function getAll();
    public static function getTableName();

}