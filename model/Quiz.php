<?php
namespace app\model;

class Quiz extends DbModel
{
    protected $id;
    protected $name;
    protected $info;
    protected $slug;


    public function __construct($name = null, $info = null, $slug = null)
    {
        $this -> name = $name;
        $this -> info = $info;
        $this -> slug = $slug;
    }

    public static function getTableName() {
        return 'quiz';
    }
}