<?php


namespace app\model;


use app\engine\Db;

class Results extends DbModel
{
    protected $id;
    protected $login;
    protected $quiz_id;
    protected $yes;
    protected $no;
    protected $hash;
    protected $created_at;


    public function __construct($login = null, $quiz_id = null, $yes = null, $no = null, $hash = null, $created_at = null)
    {

        $this->login = $login;
        $this->quiz_id = $quiz_id;
        $this->yes = $yes;
        $this->no = $no;
        $this->hash = $hash;
        $this->created_at = $created_at;
    }

    public static function getResults($user) {
        $id_quiz = $_SESSION['errors']['quiz_id'];

        //чтобы вытащить нудный id пользователя -> quiz, results
        $sql = "SELECT * FROM quiz, results WHERE results.quiz_id = quiz.id AND quiz_id = :id AND login = :login ORDER BY no";
        return Db::getInstance()->queryAll($sql, [
            'id' => $id_quiz,
            'login' => $user
        ]);
    }


    public static function getTableName() {
        return 'results';
    }
}