<?php
namespace app\model;

use app\engine\Db;

class Questions extends DbModel
{
    protected $id;
    protected $question;
    protected $quiz_id;

    public function __construct($question = null, $quiz_id = null)
    {
        $this -> question = $question;
        $this -> quiz_id = $quiz_id;
    }

    public static function getCountAll($quiz_id)
    {
        $TableName = static::getTableName();
        $sql = "SELECT COUNT(id) AS count FROM {$TableName} WHERE quiz_id = :quiz_id";
        return Db::getInstance()->queryOne($sql, ['quiz_id' => $quiz_id])['count'];
    }

    public static function getTableName()
    {
        return 'questions';
    }
}