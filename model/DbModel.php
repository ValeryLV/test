<?php
namespace app\model;

use app\interfaces\IModel;
use app\engine\Db;

abstract class DbModel extends Model
{
    //закрытое свойство для хранения экземпляра класса подключение к БД
    //protected $db;

    //вызов функции Db с созданием одного и того же экзмепляра
    //public function __construct() {
    //    $this->db = Db::getInstance();
    //}

    //получение одного вопроса по id категории в виде объекта
    public static function getOneQuestion($id)
    {
        $TableName = static::getTableName();
        $sql = "SELECT * FROM {$TableName} WHERE id = :id";
        return Db::getInstance()->queryObject($sql, ['id' => $id], static::class);
    }

    //получение 5ти вопросов по id категории в виде объекта
    public static function getRand5($id)
    {
        $TableName = static::getTableName();
        $sql = "SELECT * FROM {$TableName} WHERE id = :id ORDER BY rand() LIMIT 5";
        return Db::getInstance()->queryObjects($sql, ['id' => $id], static::class);
    }

    /**
     * @param $id
     * @return static
     */
    //чистое извлечение 1 записи, crud read
    public static function getOne($id)
    {
        $TableName = static::getTableName();
        $sql = "SELECT * FROM {$TableName} WHERE id = :id";

        return Db::getInstance()->queryObject($sql, ['id' => $id], static::class);
    }

    //получение всех категорий через статичный вызов Db::getInstance в виде массива
    public static function getAll()
    {
        $TableName = static::getTableName();
        $sql = "SELECT * FROM {$TableName}";
        return Db::getInstance()->queryObjects($sql, [], static::class);
    }

    public static function getLimitWhere($fieldName, $id, $page)
    {
        $TableName = static::getTableName();
        //Нельзя напрямую в запрос вставлять данные от пользователя
        //Вариант с неименованным плейсхолдером
        //Еще не может в обще модели быть quiz_id или любых других частностей
        $sql = "SELECT * FROM {$TableName} WHERE {$fieldName} = ? LIMIT ?, 1";
        return Db::getInstance()->queryOneLimit($sql, ['id' => $id, 'page' => $page], static::class);
    }

    public static function getAllWhere($name, $value, $options = 'default', $id = null)
    {
        $TableName = static::getTableName();
        $sql = '';
        switch ($options) {
            case 'default':
                $sql = "SELECT * FROM {$TableName} WHERE {$name} = :value";
                break;
            case 'rand':
                $sql = "SELECT * FROM {$TableName} WHERE {$name} = :value ORDER BY rand()";
                break;
            case 'like':
                $value = "%{$value}%";
                $params['id'] = $id;
                $sql = "SELECT * FROM {$TableName} WHERE {$name} LIKE :value AND quiz_id = :id";
                break;
            case 'join':
                $value = "%{$value}%";
                $params['id'] = $id;
                $sql = "SELECT a.id, a.text, a.question_id, a.is_true, q.question FROM {$TableName} AS a  JOIN questions AS q WHERE a.{$name} LIKE :value AND a.question_id = q.id AND quiz_id = :id";
                break;
        }
        $params['value'] = $value;

        return Db::getInstance()->queryObjects($sql, $params, static::class);
    }

    //имя + значение
    public static function getOneWhere($name, $value)
    {
        $TableName = static::getTableName();

        $sql = "SELECT * FROM {$TableName} WHERE {$name} = :value";

        return Db::getInstance()->queryObject($sql, ['value' => $value], static::class);
    }

    //для сохранения вызова функции
    public function save()
    {
        if (is_null($this->id))
            $this->insert();
        else
            $this->update();
    }

    //crud delete
    public function delete()
    {
        $TableName = static::getTableName();
        $sql = "DELETE FROM {$TableName} WHERE id = {$this->id}";

        //rowCount метод число затронутых строк чтобы можно было отрисовать ошибку
        return Db::getInstance()->execute($sql, ['id' => $this->id])->rowCount();
    }

    //crud insert
    public function insert()
    {
        $params = [];
        $columns = [];

        foreach ($this as $key => $value) {
            if ($key == "id") continue;
            $params[":{$key}"] = $value;
            $columns[] = "`$key`";
        }

        $columns = implode(", ", $columns);
        $values = implode(", ", array_keys($params));

        $TableName = static::getTableName();
        $sql = "INSERT INTO {$TableName} ({$columns}) VALUES ({$values})";


        Db::getInstance()->execute($sql, $params);

        $this->id = Db::getInstance()->lastInsertId();
    }

    //crud update
    public function update()
    {
        $params = [];
        $columns = [];

        foreach ($this as $key => $value) {
            //если при изменение ответа он неправильный = "0" то записываем в значение 0
            if ($value == null)
            {
                $value = 0;
            };
            $params[":{$key}"] = $value;
            $columns[] = "`$key`" . "='" . $value . "'";
        }
        $values = implode(", ", $columns);

        $TableName = static::getTableName();

        $sql = "UPDATE {$TableName} SET {$values} WHERE `id` = {$this->id}";

        Db::getInstance()->execute($sql, $params);

        $this->id = Db::getInstance()->lastInsertId();
    }

    //абстрактный метод позволяет объявить что в наследник должен
    abstract public static function getTableName();
}