<?php


namespace app\model;


class Answers extends DbModel
{
    protected $id;
    protected $text;
    protected $question_id;
    protected $is_true;

    public function __construct($text = null, $question_id = null, $is_true = null)
    {
        $this->text = $text;
        $this->question_id = $question_id;
        $this->is_true = $is_true;
    }

    public static function getTableName()
    {
        return 'answer';
    }
}