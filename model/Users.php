<?php


namespace app\model;


class Users extends DbModel
{
    protected $id = null;
    protected $login;
    protected $pass;
    protected $role;
    protected $quiz_id;

    public function __construct($login = null, $pass = null, $role = null, $quiz_id = null)
    {
        $this->login = $login;
        $this->pass = $pass;
        $this->role = $role;
        $this->quiz_id = $quiz_id;
    }

    public static function getTableName()
    {
        return 'users';
    }
}