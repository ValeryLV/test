<?php

namespace app\engine;

use app\traits\TSingletone;

class Db
{
    //подключаем TSingletone
    use TSingletone;

    private $config = [
        'driver' => 'mysql',
        'host' => 'localhost',
        'login' => 'root',
        'password' => 'root',
        'database' => 'quiz',
        'charset' => 'utf8'
    ];

//'driver' => 'mysql',
//'host' => 'localhost',
//'login' => 'logolissru_quiz',
//'password' => 'W11AfK21Quiz',
//'database' => 'logolissru_quiz',
//'charset' => 'utf8'

//$db = @mysqli_connect('localhost', 'k91542os_remont','W11AfK21','k91542os_remont') or Die(mysqli_connect_error());

    private $connection = null;

    private function getConnection() {
        if (is_null($this->connection)) {
            $this->connection = new \PDO(
                $this->prepareDSNString(),
                $this->config['login'],
                $this->config['password']
            );

            $this->connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }
        return $this->connection;
    }

    //подготовка запроса промежуточное звено (универсальный)
    private function query($sql, $params)
    {
        $pdoStatement = $this->getConnection()->prepare($sql);
        //$pdoStatement->bindValue(':id', $params['id'], \PDO::PARAM_INT);
        $pdoStatement->execute($params);
        return $pdoStatement;
    }

    private function prepareDSNString()
    {
        return sprintf("%s:host=%s;dbname=%s;charset=%s",
            $this->config['driver'],
            $this->config['host'],
            $this->config['database'],
            $this->config['charset']
        );
    }

    public function execute($sql, $params) {
        return $this->query($sql, $params);
    }

    //Кастомный запрос вида LIMIT со вставкой двух значений без скобок
    public function queryOneLimit($sql, $params, $class)
    {
        $pdoStatement = $this->getConnection()->prepare($sql);
        $pdoStatement->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);
        $pdoStatement->bindValue(1, $params['id'], \PDO::PARAM_INT);
        $pdoStatement->bindValue(2, $params['page'], \PDO::PARAM_INT);
        $pdoStatement->execute();
        return $pdoStatement->fetch();
    }

    public function lastInsertId() {
        return $this->connection->lastInsertId();
    }

    //получение всех вопросов по id категории
    public function queryOne($sql, $params = []) {
        //чтобы после запроса в query получить данные нужно извлечь их через fetch много через fetchAll
        return $this->query($sql, $params)->fetch();
    }

    //получение одного вопроса в виде объекта по его id в определенно категории
    public function queryObject($sql, $params, $class) {
        $statement= $this->query($sql, $params);
        //создаем экземпляр класса
        $statement->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);
        //возвращаем экземпляр с заполнеными данными  в виде объекта
        return $statement->fetch();
    }

    //получение всех вопросов по id категории в виде массива
    public function queryObjects($sql, $params, $class) {
        $statement= $this->query($sql, $params);
        //создаем экземпляр класса
        $statement->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);
        //возвращаем экземпляр с заполнеными данными  в виде объекта
        return $statement->fetchAll();
    }

    //получение всех категорий в виде массива
    public function queryAll($sql, $params = []) {
        return $this->query($sql, $params)->fetchAll();
    }
}