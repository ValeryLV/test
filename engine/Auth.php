<?php


namespace app\engine;

use app\model\Quiz;
use app\model\Users;


class Auth
{
    public static function Auth($login, $pass)
    {
        $user = Users::getOneWhere('login', $login);
        if (password_verify($pass, $user->pass) && !empty($user)) {
            $_SESSION['errors']['login'] = $user->login;
            $_SESSION['errors']['id'] = $user->id;
            $_SESSION['errors']['role'] = $user->role;
            $_SESSION['errors']['quiz_id'] = $user->quiz_id;
            $_SESSION['errors']['slug'] = Quiz::getOne($user->quiz_id)->slug;
            return true;
        } else {
            return false;
        }
    }

    public static function getModeratorIdQuiz()
    {
        return $_SESSION['errors']['quiz_id'];
    }

    public static function isUser()
    {
        return $_SESSION['errors']['role'] == 3;
    }

    public static function isModerator()
    {
        return $_SESSION['errors']['role'] == 2;
    }

    public static function isAdmin()
    {
        if (isset($_SESSION['errors'])) {
            return $_SESSION['errors']['role'] == 1;
        } else {
            return false;
        }

    }
}