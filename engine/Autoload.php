<?php

namespace app\engine;

class Autoload
{
    //функция для обхода папок необходимых для нахождения файла
//    private $path = [
//        'model',
//        'engine',
//        'interfaces',
//    ];

    //загрузка классов
    public function loadClass($className) {
       //foreach ($this->path as $path) {
            //$fileName = "../{$path}/{$className}.php";
            $fileName = str_replace(['app', '\\'], [ROOT_DIR, DS], $className). '.php';

            if (file_exists($fileName)) {
                include $fileName;
            }
        //}
    }
}